from employee import Employee
from dbhelper import DBHelper
from datetime import date

import calendar
import random


class Scheduler:
    def __init__(
        self,
        shift_length: int = 8,
        work_week_start: int = 0,
        work_week_end: int = 6,
    ):

        self.employee_data: list = []
        self.schedule_states: dict = {}  # for holding a weekly schedule state
        self.shift_length: int = shift_length
        self.work_week_start: int = work_week_start
        self.work_week_end: int = work_week_end

        self.cur_month: int
        self.cur_week: int

        self.cur_date: date = date.today()
        self.set_current_month(self.cur_date)
        self.set_current_week(self.cur_date.day)

    def load_from_database(self, dbname: str) -> None:
        """Load database employee data into memory"""
        dbhelper = DBHelper(dbname)
        self.employee_data: list = []

        data_records = dbhelper.get_all_records()
        for record in data_records:
            new_employee = Employee(
                record[0],  # id
                record[1],  # fname
                record[2],  # lname
                record[3],  # work position
                record[4],  # hours per week
            )

            if new_employee not in self.employee_data:
                self.employee_data.append(new_employee)

    @property
    def weeks_in_month(self) -> int:
        return len(self.cur_month)

    @property
    def total_hours_per_week(self) -> int:
        return int(((self.work_week_end - self.work_week_start) + 1) * self.shift_length)

    def set_current_month(self, date: date) -> None:
        """ Set the current month to edit -- starts on Sunday."""
        cal = calendar.Calendar(6)
        self.cur_month = cal.monthdayscalendar(date.year, date.month)

    def set_current_week(self, day: int) -> None:
        """Set the month current week -- any day in the week will work."""
        for week in self.cur_month:
            if day in week:
                self.cur_week = week

    def get_week_header(self, week_index: int) -> list:
        """Return the weekly header with month and week to the interface."""
        head = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]

        # For each day in the week
        for index in range(0, 7):
            if self.cur_week[index] == 0:
                pass
            else:
                head[index] = head[index] + " " + str(self.cur_week[index])
        head.insert(0, "ID")
        head.insert(1, "NAME")

        return head

    def get_employee(self, eid: int) -> Employee:
        """Find an employee with the id in memory."""
        for e in self.employee_data:
            if eid == e.eid:
                return e

    def available_employees(self) -> list:
        """Return a list of available employees with hours left to spare."""
        avail: list = []

        for e in self.employee_data:
            if e.hpw >= self.shift_length:
                avail.append(e)

        return avail

    def generate_week(self, emp: Employee) -> list:
        """Generate a work week for an employee, return a list of day indices."""
        week: list = []
        hours_left = emp.hpw

        while self.shift_length <= hours_left < self.total_hours_per_week:
            day = random.randint(self.work_week_start, self.work_week_end)
            if day not in week:
                week.append(day)
                hours_left = hours_left - self.shift_length

        week.sort()
        self.schedule_states[emp.eid] = week

        return week

    def remove_employee_state(self, eid: int) -> None:
        del(self.schedule_states[eid])

    def check_employee_hours(self, employee: Employee, scheduled_hours: int) -> str:
        """Verify employee still has hours, and send a notification if under or over scheduled."""
        notification: str = None 

        if scheduled_hours < employee.hpw:
            notification = (
                str(
                    "Employee ["
                    + str(employee.eid)
                    + "] - "
                    + employee.first_name
                    + " "
                    + employee.last_name
                    + " has "
                    + str(employee.hpw - scheduled_hours)
                    + " unscheduled hours!"
                )
            )

        if employee.hpw > self.total_hours_per_week:
            notification = (
                str(
                    "Employee ["
                    + str(employee.eid)
                    + "] - "
                    + employee.first_name
                    + " "
                    + employee.last_name
                    + " is overscheduled by "
                    + str(employee.hpw - self.total_hours_per_week) 
                    + " hours!"
                )
            )

        return notification

    def check_schedule(self) -> list:
        """Handle hour discrepancies and remove employees from memory if needed."""
        notifications: list = []
        for emp_id, emp_week in self.schedule_states.items():
            employee: Employee = self.get_employee(emp_id)
            scheduled_hours: int = len(emp_week) * self.shift_length

            if scheduled_hours > self.total_hours_per_week or employee is None :
                self.remove_employee_state(emp_id)
                break
            else:
                notifications.append(self.check_employee_hours(employee, scheduled_hours ))

        return [n for n in notifications if n is not None]

    def update_date(self, date: date) -> None:
        """Update the day, week, and month."""
        self.cur_date = date
        self.set_current_month(date)
        self.set_current_week(self.cur_date.day)
