from dataclasses import dataclass


@dataclass
class Employee:
    eid: int
    first_name: str
    last_name: str
    work_position: list
    hpw: int  # hours per week
