import npyscreen
import sys

from scheduler import Scheduler
from dbhelper import DBHelper


class BoxedWidgetText(npyscreen.BoxTitle):
    _contained_widget = npyscreen.Pager


class MainScreenForm(npyscreen.FormWithMenus):
    def create(self) -> None:
        self.name : str = "Scheduler"

        self.scheduler: Scheduler = Scheduler(work_week_start=1)
        self.scheduler.load_from_database("employee.db")

        self.keypress_timeout: int = 1  # refresh rate of form widgets
        self.week_schedule: list = []

        self.notification_widget = self.add(
            BoxedWidgetText, name="Notifications", width=80, height=5
        )

        self.date_select_field = self.add(
            npyscreen.TitleDateCombo,
            name="Week of ",
            value=self.scheduler.cur_date,
            rely=8
        )

        self.generate_schedule_button = self.add(
            npyscreen.ButtonPress,
            name="Generate Schedule",
            when_pressed_function=self.generate_schedule,
            relx=0,
            rely=12,
        )

        self.calendar = self.add(
            npyscreen.GridColTitles,
            name="Week",
            rely=15,
            columns=9,
            col_margin=1,
            row_height=2,
            values=[""],
            select_whole_line=True,
            col_titles=self.scheduler.get_week_header(self.scheduler.cur_week),
        )
        self.employee_menu = self.new_menu(name="Employee")
        self.employee_menu.addItem(text="Add Employee", onSelect=AddEmployeeForm)
        self.employee_menu.addItem(text="Update Employee", onSelect=UpdateEmployeeForm)
        self.employee_menu.addItem(text="Remove Employee", onSelect=RemoveEmployeeForm)
        self.employee_menu.addItem(text="View Employee", onSelect=ViewEmployeeForm)

    def generate_schedule(self) -> None:
        self.week_schedule.clear()
        self.calendar.values.clear()
        self.scheduler.load_from_database("employee.db")

        # Generate the schedule

        if len(self.scheduler.employee_data) <= 0:
            pass

        for e in self.scheduler.employee_data:
            emp: list = [e.eid]
            emp.append(e.first_name + " " + e.last_name)
            emp_ww: list = self.scheduler.generate_week(e)

            for day in range(0, 7):
                if day in emp_ww:
                    emp.append(e.work_position)
                else:
                    emp.append("")
            self.week_schedule.append(emp)

        self.notification_widget.values.clear()
        for n in self.scheduler.check_schedule():
            self.notification_widget.values.append(n)

        self.scheduler.update_date(self.date_select_field.value)
        self.calendar.col_titles = self.scheduler.get_week_header(
            self.scheduler.cur_week
        )
        self.refresh_screen()

    def refresh_screen(self) -> None:
        self.calendar.values = self.week_schedule

    def afterEditing(self) -> None:
        self.parentApp.setNextForm(None)
        sys.exit(0)

    def while_waiting(self) -> None:
        self.refresh_screen()
        self.display()


class AddEmployeeForm(npyscreen.Popup):
    def create(self):

        self.name = "Add Employee"
        self.first_name = self.add(npyscreen.TitleText, name="First Name: ")
        self.last_name = self.add(npyscreen.TitleText, name="Last Name: ")
        self.work_position = self.add(npyscreen.TitleText, name="Work_Position: ")
        self.hours = self.add(npyscreen.TitleText, name="Work Hours: ")

        self.edit()

        try:
            self.hours.value = int(self.hours.value)
        except ValueError:
            self.hours.value = 1

        if self.hours.value < 0:
            self.hours.value = 0

        dbhelper = DBHelper("employee.db")
        dbhelper.add_record(
            [
                self.first_name.value,
                self.last_name.value,
                self.work_position.value,
                self.hours.value,
            ]
        )


class RemoveEmployeeForm(npyscreen.Popup):
    def create(self):
        self.name = "Remove Employee"
        self.id_index = self.add(npyscreen.TitleText, name="ID Index: ")

        self.edit()

        dbhelper = DBHelper("employee.db")
        dbhelper.remove_record(self.id_index.value)


class UpdateEmployeeForm(npyscreen.Popup):
    def create(self) -> None:
        self.name = "Update Employee"

        self.id_index = self.add(npyscreen.TitleText, name="ID Index to Update: ")
        self.first_name = self.add(npyscreen.TitleText, name="First Name: ")
        self.last_name = self.add(npyscreen.TitleText, name="Last Name: ")
        self.work_position = self.add(npyscreen.TitleText, name="Work Position: ")
        self.hours = self.add(npyscreen.TitleText, name="Hours: ")

        self.edit()

        try:
            self.id_index.value = int(self.id_index.value)
        except ValueError:
            self.id_index.value = 0

        try:
            self.hours.value = int(self.hours.value)
        except ValueError:
            self.hours.value = 1


        dbhelper = DBHelper("employee.db")
        dbhelper.update_record(
            [
                self.id_index.value,
                self.first_name.value,
                self.last_name.value,
                self.work_position.value,
                self.hours.value,
            ]
        )


class ViewEmployeeForm(npyscreen.Form):
    def create(self) -> None:

        self.name = "View Employee"

        scheduler = Scheduler()

        scheduler.load_from_database("employee.db")
        self.eid = self.add(npyscreen.TitleText, name="Employee ID: ")

        self.edit()

        try:
            e = scheduler.get_employee(int(self.eid.value))

            self.first_name = self.add(
                npyscreen.TitleText, name="First Name: " + str(e.first_name)
            )
            self.last_name = self.add(
                npyscreen.TitleText, name="Last Name: " + str(e.last_name)
            )
            self.work_position = self.add(
                npyscreen.TitleText, name="Work Position: " + str(e.work_position)
            )
            self.hours = self.add(npyscreen.TitleText, name="Hours: " + str(e.hpw))

        except AttributeError:
            return
        except ValueError:
            return
        self.edit()


class Application(npyscreen.NPSAppManaged):
    def onStart(self):
        self.addForm("MAIN", MainScreenForm, name="Scheduler")
