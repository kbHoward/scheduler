from os import path

from interface import Application
from dbhelper import DBHelper

if __name__ == "__main__":

    # Create the database if it dosen't exist
    if not path.exists("employee.db"):
        dbhelper : DBHelper = DBHelper("employee.db")
        dbhelper.create_table()

        employee_data: list = []

        employee_data.append(("John", "Doe", "Reference", 20))
        employee_data.append(("Jane", "Doe", "Marketing", 45))
        employee_data.append(("Steve", "Doe", "Sales", 20))
        employee_data.append(("Emily", "Doe", "Director", 40))

        dbhelper.add_records(employee_data)

    app = Application().run()
