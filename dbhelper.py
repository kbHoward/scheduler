import sqlite3

class DBHelper:

    # Connect to the database
    def __init__(self, db_name):
        self.db_name = db_name

    # Create the default database table, employee, if it does not exist
    def create_table(self) -> None:
        con = sqlite3.connect(self.db_name)
        cur = con.cursor()
        cur.execute(
            '''SELECT count(name) FROM sqlite_master WHERE type="table" AND name="employees"'''
        )
        if not cur.fetchone()[0]:
            cur.execute(
                """CREATE TABLE employees(emp_id INTEGER PRIMARY KEY, firstname TEXT,lastname TEXT,work_position
                TEXT, hours INT) """
            )
        con.commit()
        con.close()

    # Add multiple record to a the employee table, record_data is a multi record list
    def add_records(self, record_data: list) -> None:
        con = sqlite3.connect(self.db_name)
        cur = con.cursor()
        cur.executemany(
            """INSERT INTO employees(firstname, lastname, work_position, hours) VALUES(?,?,?,?)""",
            record_data,
        )
        con.commit()
        con.close()

    # Add a single record to the employee table, values are passed through as a list.
    def add_record(self, record: list) -> None:
        con = sqlite3.connect(self.db_name)
        cur = con.cursor()
        cur.execute(
            """INSERT INTO employees(firstname, lastname, work_position, hours) VALUES(?,?,?,?)""",
            record,
        )
        con.commit()
        con.close()

    # Update a record in the employee table. Primary ID key is supplied last.
    def update_record(self, record: list) -> None:
        con = sqlite3.connect(self.db_name)
        cur = con.cursor()
        cur.execute(
            """UPDATE employees SET firstname=?,lastname=?,work_position=?,hours=? WHERE emp_id=?""",
            (record[1], record[2], record[3], record[4], record[0]),
        )
        con.commit()
        con.close()

    # Remove a record from the employee table. Primary key of record is given
    def remove_record(self, eid: str) -> None:
        con = sqlite3.connect(self.db_name)
        cur = con.cursor()
        con.row_factory = sqlite3.Row
        for row in con.execute("SELECT * FROM employees"):
            con.execute("""DELETE FROM employees WHERE emp_id=?""", (eid,))
        con.commit()
        con.close()

    # Get a record from the employee table.
    def get_record(self, index: int) -> None:
        con = sqlite3.connect(self.db_name)
        cur = con.cursor()
        con.row_factory = sqlite3.Row
        cur.execute("""SELECT emp_id FROM employees WHERE emp_id=?""", (index,))
        con.commit()
        con.close()

    # Get all records from the employee tables.
    def get_all_records(self) -> list:
        con = sqlite3.connect(self.db_name)
        cur = con.cursor()
        rows : list = []

        for row in cur.execute("""SELECT * FROM employees"""):
            rows.append(row)
        con.commit()
        con.close()
        return rows
