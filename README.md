# Scheduler

![Main Form](sch1.jpg)

## About
scheduler is a TUI program that allows for the creation and automatic generation of schedules.

## Features
* Week-by-week view of schedule. 
* Simple generation of schedule automatically.
* Notifications for conflicts.
* Easy and simple menu for adding records to SQLite.

### Dependencies
* python3.7+
* python venv
* pip3
* npyscreen
* git (optional)

## Setup Virtual Environment
### Linux Installation
1. Download the zip file or clone the repository like below (requires git).

2. `git clone https://gitlab.com/kbhoward/scheduler` 

3. `python3 -m venv /path/to/the/download/directory` 

4. `cd scheduler`

5. `pip install -r requirements.txt`

6. `python main.py`

### Windows 10 Installation

1. Install the latest version of python3 and ensure that it is added to your PATH. This should install the latest version of Python 3 and pip to be used in a command prompt.
2. Install windows-curses with pip through a command prompt using the following command:

`python -m pip install windows-curses`

3. `python3 -m venv \path\to\the\download\directory` 

4. `cd \path\to\the\download\directory`

5. `pip install -r requirements.txt`

6. `python main.py`

## Usage
* Use the keyboard to navigate to the 'Generate Schedule' button after making changes to records or selecting the date of a week.
* The TAB key switches between widget forms.
* Ctrl+X on the keyboard opens up the pop up form for adding, removing, updating, and viewing records.
* CTRL+P cycles towards upwards widgets, while CTRL+N cycles downwards.
* Notification panel allows for display of hour allotment discrepencies that have been found by the scheduler, this panel is scrollable, and can be exited by the TAB key.
* 'j' and 'k' keys on the keyboard as an alternative for the up and down arrow keys.

![Employee Menu](sch2.jpg)
